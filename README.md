# Code for producing figure of Mapping the race between crop's phenology and climate risks. 


## Dataset required

You need to download the whole file at (link to the dataset). All the metada of this dataset are provided.  
All the ecoclimatics indicators have been created with GETARI software (García de Cortázar-Atauri, Iñaki ; Maury, Olivier, 2019, “GETARI : Generic Evaluation Tool of AgRoclimatic Indicators”, DOI 10.15454/IZUFAP, Portail Data INRAE, V1) and SEASON software (Maury, Olivier; Garcia de Cortazar Atauri, Iñaki; Bertuzzi, Patrick; Lagier, Marc; Clastre, Philippe, 2021, "SEASON : System for Evaluation of Agriculture faiSability using indicatOrs combiNation", https://doi.org/10.15454/LAPNHT, Recherche Data Gouv, V3 )

## Code description

The different code are numbered 0 to 3, and all use functions written in the function.R file.

### Phenological maps historic

This code takes phenological files for the 3 models then extract the reference periode (1991-2020) the produce the maps presented in figure 2

### Clustering
This code performed clustering on indicators of the three models then maps it like in figure 3

### Future phenology
This code allow to produce density plot (Figure 4)

### Evoution of risk and bivariate map
This code produce figure 5 and 6



